﻿using CoreWebApi.Controllers;
using CoreWebApi.Models;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace CoreWebApi.Tests.Controllers
{
    public class ActionsControllerTest
    {
        [Fact]
        public void GetAllActions()
        {
            var moc = new Mock<IActionRepository>();
            moc.Setup(m => m.ShowAllActions()).Returns(new List<Models.Action>() { new Models.Action() { Id = 1}, new Models.Action() { Id = 2 }, new Models.Action() { Id = 3 } });

            var actController = new ActionsController(moc.Object);

            var result = actController.GetAllActions();

            Assert.Equal<int>(3, result.Count());
        }

        [Fact]
        public void GetAllActionsVal()
        {
            var moc = new Mock<IActionRepository>();
            moc.Setup(m => m.ShowAllActions()).Returns(new List<Models.Action>() { new Models.Action() { Id = 3 }, new Models.Action() { Id = 4 }, new Models.Action() { Id = 5 } });

            var actController = new ActionsController(moc.Object);

            var result = actController.GetAllActions();

            Assert.Equal<int>(3, (result.ToList())[0].Id);
            Assert.Equal<int>(4, (result.ToList())[1].Id);
            Assert.Equal<int>(5, (result.ToList())[2].Id);
        }

        [Fact]
        public void GetAction()
        {
            var moc = new Mock<IActionRepository>();
            moc.Setup(m => m.GetAction(5)).Returns(new Models.Action() { Id = 5, ActionResult = 14, CurrentAction = "addition", Param1 = 10, param2 = 4 });

            var actController = new ActionsController(moc.Object);

            var result = actController.GetAction(5);

            Assert.Equal(5, result.Id);
            Assert.Equal(14, result.ActionResult);
            Assert.Equal("addition", result.CurrentAction);
        }

        [Fact]
        public void GetActionId()
        {
            var moc = new Mock<IActionRepository>();
            moc.Setup(m => m.SetAction(It.IsAny<Models.Action>())).Returns(7);

            var actController = new ActionsController(moc.Object);

            var result = actController.GetActionId("multiplication", 4, 6);

            Assert.Equal(7, result);
        }
    }
}
