﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWebApi.Models
{
    public class ActionsDBContext : IActionRepository
    {
        private ActionContext db;

        public ActionsDBContext()
        {
            var optionsBuilder = new DbContextOptionsBuilder<ActionContext>();

            var options = optionsBuilder
                    .UseSqlServer("Server=(localdb)\\mssqllocaldb;Database=actiondb;Trusted_Connection=True;MultipleActiveResultSets=true")
                    .Options;

            db = new ActionContext(options);
        }

        public Action GetAction(int id)
        {
            return db.Actions.Find(id);
        }

        public int SetAction(Action action)
        {
            db.Actions.Add(action);
            db.SaveChanges();
            return action.Id;
        }

        public IEnumerable<Action> ShowAllActions()
        {
            return db.Actions;
        }
    }
}
