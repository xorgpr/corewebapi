﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWebApi.Models
{
    public class ActionContext : DbContext
    {
        public DbSet<Action> Actions { get; set; }
        
        public ActionContext(DbContextOptions<ActionContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
