﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWebApi.Models
{
    public interface IActionRepository
    {
        IEnumerable<Models.Action> ShowAllActions();
        Models.Action GetAction(int id);
        int SetAction(Models.Action action);
    }
}
