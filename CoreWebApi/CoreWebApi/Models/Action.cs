﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoreWebApi.Models
{
    public class Action
    {
        public int Id { get; set; }
        public string CurrentAction { get; set; }
        public int Param1 { get; set; }
        public int param2 { get; set; }
        public int ActionResult { get; set; } 
    }
}
