﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoreWebApi.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoreWebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Actions")]
    public class ActionsController : Controller
    {
        private readonly IActionRepository dbRepo;
        public ActionsController(IActionRepository context)
        {
            dbRepo = context;
        }

        [HttpGet]
        public IEnumerable<Models.Action> GetAllActions()
        {
            return dbRepo.ShowAllActions();
        }

        [HttpGet("{id:int}")]
        public Models.Action GetAction(int id)
        {
            return dbRepo.GetAction(id);
        }

        [HttpGet("{operation:regex(^(addition|subtraction|multiplication|division|exponentiation)$)}/{param1:regex(^[[0-9]]+$)}/{param2:regex(^[[0-9]]+$)}")]
        public int GetActionId(string operation, int param1, int param2)
        {
            int result = 0;

            try
            {
                switch (operation)
                {
                    case "addition":
                        {
                            result = param1 + param2;
                        }
                        break;
                    case "subtraction":
                        {
                            result = param1 - param2;
                        }
                        break;
                    case "multiplication":
                        {
                            result = param1 * param2;
                        }
                        break;
                    case "division":
                        {
                            result = param1 / param2;
                        }
                        break;
                    case "exponentiation":
                        {
                            result = (int)Math.Pow(param1, param2);
                        }
                        break;
                    default:
                        {
                            result = 0;
                        }
                        break;
                }
            }
            catch(Exception ex)
            {
                result = 0;
            }

            var currentAction = new Models.Action()
            {
                CurrentAction = operation,
                Param1 = param1,
                param2 = param2,
                ActionResult = result
            };
            
            return dbRepo.SetAction(currentAction);
        }
    }
}